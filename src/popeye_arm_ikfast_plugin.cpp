/*********************************************************************
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2013, Dave Coleman, CU Boulder; Jeremy Zoss, SwRI; David Butterworth, KAIST; Mathias Lüdtke, Fraunhofer
 *IPA
 * All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the all of the author's companies nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *********************************************************************/

#include <ros/ros.h>
#include <moveit/kinematics_base/kinematics_base.h>
#include <urdf/model.h>
#include <tf_conversions/tf_kdl.h>
#include <eigen_conversions/eigen_msg.h>
#include <eigen_conversions/eigen_kdl.h>
#include <ikfast.h> // found inside share/openrave-X.Y/python/ikfast.h
#include <random_numbers/random_numbers.h>

// ROS msgs
#include <geometry_msgs/PoseStamped.h>
#include <moveit_msgs/MoveItErrorCodes.h>

// KDL
#include <kdl/jntarray.hpp>
#include <moveit/kdl_kinematics_plugin/chainiksolver_vel_pinv_mimic.hpp>
#include <moveit/kdl_kinematics_plugin/joint_mimic.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>

// MoveIt!
//#include <moveit/kinematics_base/kinematics_base.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>

// URDF, SRDF
#include <srdfdom/model.h>
#include <moveit/rdf_loader/rdf_loader.h>

#include <Eigen/Dense>


// Need a floating point tolerance when checking joint limits, in case the joint starts at limit
const double LIMIT_TOLERANCE = .0000001;
/// \brief Search modes for searchPositionIK(), see there
enum SEARCH_MODE
{
  OPTIMIZE_FREE_JOINT = 1,
  OPTIMIZE_MAX_JOINT = 2
};

namespace ikfast_kinematics_plugin
{
#define IKFAST_NO_MAIN  // Don't include main() from IKFast


// struct for storing and sorting solutions
struct LimitObeyingSol
{
  std::vector<double> value;
  double dist_from_seed;

  bool operator<(const LimitObeyingSol& a) const
  {
    return dist_from_seed < a.dist_from_seed;
  }
};

inline double IKatan2 ( double fy, double fx )
{
    if ( isnan ( fy ) )
    {
        assert ( !std::isnan ( fx ) ); // if both are nan, probably wrong value will be returned
        return M_PI/2.0;
    }
    else if ( isnan ( fx ) )
    {
        return 0;
    }
    return std::atan2 ( fy,fx );
}

  ///////////////////////////////////////////////////////////////////////////////////////


class IKFastKinematicsPlugin : public kinematics::KinematicsBase
{
  const std::string NAME_              = "ikfast";
  const std::string IKFAST_BASE_FRAME_ = "popeye_link_0";
  const std::string IKFAST_EE_FRAME_   = "popeye_flange"; 
  const int         POPEYE_NON_REDUNDANT_DEGREES_OF_FREEDOM_ = 4;

  std::vector<std::string>  all_joint_names_;
  std::vector<std::string>  all_link_names_;
  std::vector<std::string>  joint_names_;
  std::vector<double>       joint_min_vector_;
  std::vector<double>       joint_max_vector_;
  std::vector<bool>         joint_has_limits_vector_;
  std::vector<std::string>  link_names_;
  std::vector<int>          free_params_;
  bool                      active_;  // Internal variable that indicates whether solvers are configure1d and ready
  
  
  int  getNumJoints() const { return POPEYE_NON_REDUNDANT_DEGREES_OF_FREEDOM_; }
  int  getNumFreeParameters() const { return 0; }
  int* getFreeParameters() const { return nullptr; }

  const std::vector<std::string>& getJointNames() const
  {
    return joint_names_;
  }
  const std::vector<std::string>& getLinkNames() const
  {
    return link_names_;
  }
  int getJointIndex(const std::string &name) const
  {
    for (unsigned int i=0; i < all_joint_names_.size(); i++) {
      if (joint_names_[i] == name)
        return i;
    }
    return -1;
  }
  bool ComputeIk ( const double px, const double py, const double pz, const double alpha, ikfast::IkSolutionListBase<double>& solutions ) const
  {
    std::vector< KDL::Frame >  frames;
    for( size_t i=0; i<chain_.getNrOfSegments(); i++ )
    {
      frames.push_back( chain_.getSegment(i).getFrameToTip() );
    }

    double h0 = frames[0].p.z();
    double o1 = frames[1].p.y();
    double l2 = std::fabs( frames[2].p.z() );
    double l3 = std::fabs( frames[3].p.z() );
    double o3 = frames[3].p.y();
        
    double xp = -(pz-h0);
    double yp = px;

    solutions.Clear();
    double q3[2] = {0};
    double q2[2] = {0};
    double q2b[2] = {0};
    double M = ( std::pow(xp,2) + std::pow(yp,2) - std::pow(l2,2) - std::pow(l3,2) ) / ( 2.0 * l2 * l3 );
    q3[0] =  std::acos( M );
    q3[1] = -std::acos( M );
    
    for( size_t i=0; i<2;i++)
    {
      double s3 =  std::sin( q3[i] );
      double c3 =  std::cos( q3[i] );
      double c2 =  ( xp * (l2 + l3*c3 ) + yp * l3 * s3 ) / ( std::pow(xp,2) + std::pow(yp,2) );
      double s2 =  ( yp * (l2 + l3*c3 ) - xp * l3 * s3 ) / ( std::pow(xp,2) + std::pow(yp,2) );
      q2[i]  = std::atan2( s2, c2 ); 
      q2b[i] = std::atan2( c2, s2 ); 
    }
    
    // sol 1
    {
      std::vector< ikfast::IkSingleDOFSolutionBase<double> > vinfos ( 4 );
      vinfos[0].jointtype    = 0x11;
      vinfos[0].foffset      = py-(o1 + o3);
      vinfos[0].indices[0]   = 1;
      vinfos[0].indices[1]   = 2;
      vinfos[0].maxsolutions = 2;
      vinfos[1].jointtype    = 1;
      vinfos[1].foffset      = q2[0];
      vinfos[1].indices[0]   = 1;
      vinfos[1].indices[1]   = 2;
      vinfos[1].maxsolutions = 2;
      vinfos[2].jointtype    = 0x01;
      vinfos[2].foffset      = q3[0];
      vinfos[2].indices[0]   = 1;
      vinfos[2].indices[1]   = 2;
      vinfos[2].maxsolutions = 2;
      vinfos[3].jointtype    = 0x01;
      vinfos[3].foffset      = alpha - vinfos[1].foffset - vinfos[2].foffset;
      vinfos[3].indices[0]   = 1;
      vinfos[3].indices[1]   = 2;
      vinfos[3].maxsolutions = 2;
      std::vector<int> vfree; vfree.clear();
      solutions.AddSolution ( vinfos,vfree );
    }
    
    // sol 2
    {
      std::vector< ikfast::IkSingleDOFSolutionBase<double> > vinfos ( 4 );
      vinfos[0].jointtype    = 0x11;
      vinfos[0].foffset      = py-(o1 + o3);;
      vinfos[0].indices[0]   = 1;
      vinfos[0].indices[1]   = 2;
      vinfos[0].maxsolutions = 2;
      vinfos[1].jointtype    = 1;
      vinfos[1].foffset      = q2[1];
      vinfos[1].indices[0]   = 1;
      vinfos[1].indices[1]   = 2;
      vinfos[1].maxsolutions = 2;
      vinfos[2].jointtype    = 0x01;
      vinfos[2].foffset      = q3[1];
      vinfos[2].indices[0]   = 1;
      vinfos[2].indices[1]   = 2;
      vinfos[2].maxsolutions = 2;
      vinfos[3].jointtype    = 0x01;
      vinfos[3].foffset      = alpha - vinfos[1].foffset - vinfos[2].foffset;
      vinfos[3].indices[0]   = 1;
      vinfos[3].indices[1]   = 2;
      vinfos[3].maxsolutions = 2;
      std::vector<int> vfree; vfree.clear();
      solutions.AddSolution ( vinfos,vfree );
    }
    
    return true;
  }
  void ComputeFk ( const Eigen::VectorXd& j, Eigen::Affine3d& T_ib_it  ) const
  {
    ROS_DEBUG_STREAM( "[ComputeFk] q IN: " << j.transpose() );
    assert( j.rows() == chain_.getNrOfJoints() );
    
    KDL::JntArray q(chain_.getNrOfJoints()); 
    for( size_t i=0; i<chain_.getNrOfJoints(); i++) 
    {
      q(i) = j(i);
    }
    
    std::vector< KDL::Frame >  frames;
    for( const auto & segment : chain_.segments )
    {
      frames.push_back( segment.getFrameToTip() );
      KDL::Joint joint = segment.getJoint();
    }

    KDL::ChainFkSolverPos_recursive fksolver(chain_);

    KDL::Frame ee_frame;
    fksolver.JntToCart(q, ee_frame);
    tf::transformKDLToEigen(ee_frame, T_ib_it);
    
    ROS_DEBUG_STREAM( "[ComputeFk] FK OUT:\n Translation:" << T_ib_it.translation() << "\n Rotation Matrix:\n" << T_ib_it.linear() );
    return;
  }
  
  robot_model::RobotModelPtr    robot_model_;
  robot_model::JointModelGroup* joint_model_group_;
  KDL::Tree                     tree_;
  KDL::Chain                    chain_;

  // 0: world frame
  // gb: group base frame
  // gt: group tip  frame
  // ib: ikfast base frame (i.e., autogenerated file in such frame)
  // it: ikfast tip frame (i.e., autogenerated file in such frame)
  Eigen::Affine3d T_gb_ib_;
  Eigen::Affine3d T_gt_it_;


public:
  /** @class
   *  @brief Interface for an IKFast kinematics plugin
   */
  IKFastKinematicsPlugin() : active_(false)
  {
    srand(time(NULL));
    supported_methods_.push_back(kinematics::DiscretizationMethods::NO_DISCRETIZATION);
    supported_methods_.push_back(kinematics::DiscretizationMethods::ALL_DISCRETIZED);
    supported_methods_.push_back(kinematics::DiscretizationMethods::ALL_RANDOM_SAMPLED);
  }

  /**
   * @brief Given a desired pose of the end-effector, compute the joint angles to reach it
   * @param ik_pose the desired pose of the link
   * @param ik_seed_state an initial guess solution for the inverse kinematics
   * @param solution the solution vector
   * @param error_code an error code that encodes the reason for failure or success
   * @return True if a valid solution was found, false otherwise
   */

  // Returns the IK solution that is within joint limits closest to ik_seed_state
  bool getPositionIK(const geometry_msgs::Pose& ik_pose, const std::vector<double>& ik_seed_state,
                     std::vector<double>& solution, moveit_msgs::MoveItErrorCodes& error_code,
                     const kinematics::KinematicsQueryOptions& options = kinematics::KinematicsQueryOptions()) const;

  /**
   * @brief Given a desired pose of the end-effector, compute the set joint angles solutions that are able to reach it.
   *
   * This is a default implementation that returns only one solution and so its result is equivalent to calling
   * 'getPositionIK(...)' with a zero initialized seed.
   *
   * @param ik_poses  The desired pose of each tip link
   * @param ik_seed_state an initial guess solution for the inverse kinematics
   * @param solutions A vector of vectors where each entry is a valid joint solution
   * @param result A struct that reports the results of the query
   * @param options An option struct which contains the type of redundancy discretization used. This default
   *                implementation only supports the KinmaticSearches::NO_DISCRETIZATION method; requesting any
   *                other will result in failure.
   * @return True if a valid set of solutions was found, false otherwise.
   */
  bool getPositionIK(const std::vector<geometry_msgs::Pose>& ik_poses, const std::vector<double>& ik_seed_state,
                     std::vector<std::vector<double>>& solutions, kinematics::KinematicsResult& result,
                     const kinematics::KinematicsQueryOptions& options) const;

  /**
   * @brief Given a desired pose of the end-effector, search for the joint angles required to reach it.
   * This particular method is intended for "searching" for a solutions by stepping through the redundancy
   * (or other numerical routines).
   * @param ik_pose the desired pose of the link
   * @param ik_seed_state an initial guess solution for the inverse kinematics
   * @return True if a valid solution was found, false otherwise
   */
  bool searchPositionIK(const geometry_msgs::Pose& ik_pose, const std::vector<double>& ik_seed_state, double timeout,
                        std::vector<double>& solution, moveit_msgs::MoveItErrorCodes& error_code,
                        const kinematics::KinematicsQueryOptions& options = kinematics::KinematicsQueryOptions()) const;

  /**
   * @brief Given a desired pose of the end-effector, search for the joint angles required to reach it.
   * This particular method is intended for "searching" for a solutions by stepping through the redundancy
   * (or other numerical routines).
   * @param ik_pose the desired pose of the link
   * @param ik_seed_state an initial guess solution for the inverse kinematics
   * @param the distance that the redundancy can be from the current position
   * @return True if a valid solution was found, false otherwise
   */
  bool searchPositionIK(const geometry_msgs::Pose& ik_pose, const std::vector<double>& ik_seed_state, double timeout,
                        const std::vector<double>& consistency_limits, std::vector<double>& solution,
                        moveit_msgs::MoveItErrorCodes& error_code,
                        const kinematics::KinematicsQueryOptions& options = kinematics::KinematicsQueryOptions()) const;

  /**
   * @brief Given a desired pose of the end-effector, search for the joint angles required to reach it.
   * This particular method is intended for "searching" for a solutions by stepping through the redundancy
   * (or other numerical routines).
   * @param ik_pose the desired pose of the link
   * @param ik_seed_state an initial guess solution for the inverse kinematics
   * @return True if a valid solution was found, false otherwise
   */
  bool searchPositionIK(const geometry_msgs::Pose& ik_pose, const std::vector<double>& ik_seed_state, double timeout,
                        std::vector<double>& solution, const IKCallbackFn& solution_callback,
                        moveit_msgs::MoveItErrorCodes& error_code,
                        const kinematics::KinematicsQueryOptions& options = kinematics::KinematicsQueryOptions()) const;

  /**
   * @brief Given a desired pose of the end-effector, search for the joint angles required to reach it.
   * This particular method is intended for "searching" for a solutions by stepping through the redundancy
   * (or other numerical routines).  The consistency_limit specifies that only certain redundancy positions
   * around those specified in the seed state are admissible and need to be searched.
   * @param ik_pose the desired pose of the link
   * @param ik_seed_state an initial guess solution for the inverse kinematics
   * @param consistency_limit the distance that the redundancy can be from the current position
   * @return True if a valid solution was found, false otherwise
   */
  bool searchPositionIK(const geometry_msgs::Pose& ik_pose, const std::vector<double>& ik_seed_state, double timeout,
                        const std::vector<double>& consistency_limits, std::vector<double>& solution,
                        const IKCallbackFn& solution_callback, moveit_msgs::MoveItErrorCodes& error_code,
                        const kinematics::KinematicsQueryOptions& options = kinematics::KinematicsQueryOptions()) const;

  /**
   * @brief Given a set of joint angles and a set of links, compute their pose
   *
   * @param link_names A set of links for which FK needs to be computed
   * @param joint_angles The state for which FK is being computed
   * @param poses The resultant set of poses (in the frame returned by getBaseFrame())
   * @return True if a valid solution was found, false otherwise
   */
  bool getPositionFK(const std::vector<std::string>& link_names, const std::vector<double>& joint_angles,
                     std::vector<geometry_msgs::Pose>& poses) const;

  /**
   * @brief Sets the discretization value for the redundant joint.
   *
   * Since this ikfast implementation allows for one redundant joint then only the first entry will be in the
   *discretization map will be used.
   * Calling this method replaces previous discretization settings.
   *
   * @param discretization a map of joint indices and discretization value pairs.
   */
  void setSearchDiscretization(const std::map<int, double>& discretization);

  /**
   * @brief Overrides the default method to prevent changing the redundant joints
   */
  bool setRedundantJoints(const std::vector<unsigned int>& redundant_joint_indices);

//private:
  bool initialize(const std::string& robot_description, const std::string& group_name, const std::string& base_name,
                  const std::string& tip_name, double search_discretization);

  
  private:
  /**
   * @brief Calls the IK solver from IKFast
   * @return The number of solutions found
   */
  int solve(KDL::Frame& pose_frame, const std::vector<double>& vfree, ikfast::IkSolutionList<double>& solutions) const;

  /**
   * @brief Gets a specific solution from the set
   */
  void getSolution(const ikfast::IkSolutionList<double>& solutions, int i, std::vector<double>& solution) const;

  /**
   * @brief Gets a specific solution from the set with joints rotated 360° to be near seed state where possible
   */
  void getSolution(const ikfast::IkSolutionList<double>& solutions, const std::vector<double>& ik_seed_state, int i,
                   std::vector<double>& solution) const;

  double harmonize(const std::vector<double>& ik_seed_state, std::vector<double>& solution) const;
  // void getOrderedSolutions(const std::vector<double> &ik_seed_state, std::vector<std::vector<double> >& solslist);
  void getClosestSolution(const ikfast::IkSolutionList<double>& solutions, const std::vector<double>& ik_seed_state,
                          std::vector<double>& solution) const;
  void fillFreeParams(int count, int* array);
  bool getCount(int& count, const int& max_count, const int& min_count) const;

  /**
  * @brief samples the designated redundant joint using the chosen discretization method
  * @param  method              An enumeration flag indicating the discretization method to be used
  * @param  sampled_joint_vals  Sampled joint values for the redundant joint
  * @return True if sampling succeeded.
  */
  bool sampleRedundantJoint(kinematics::DiscretizationMethod method, std::vector<double>& sampled_joint_vals) const;

};  // end class

bool IKFastKinematicsPlugin::initialize(const std::string& robot_description, const std::string& group_name,
                                        const std::string& base_name, const std::string& tip_name,
                                        double search_discretization)
{
  setValues(robot_description, group_name, base_name, tip_name, search_discretization);

  ros::NodeHandle node_handle("~/" + group_name);

  std::string robot;
  lookupParam("robot", robot, std::string());

  redundant_joint_indices_.clear();
  
  std::string xml_string;
  std::string urdf_xml, full_urdf_xml;
  lookupParam("urdf_xml", urdf_xml, robot_description);
  node_handle.searchParam(urdf_xml, full_urdf_xml);

  ROS_DEBUG_NAMED(NAME_, "Reading xml file from parameter server");
  if (!node_handle.getParam(full_urdf_xml, xml_string))
  {
    ROS_FATAL_NAMED(NAME_, "Could not load the xml from parameter server: %s", urdf_xml.c_str());
    return false;
  }

  rdf_loader::RDFLoader rdf_loader(robot_description_);
  const std::shared_ptr<srdf::Model>&           srdf       = rdf_loader.getSRDF();
  const std::shared_ptr<urdf::ModelInterface>&  urdf_model = rdf_loader.getURDF();
  if (!urdf_model || !srdf)
  {
    ROS_ERROR_NAMED("kdl","URDF and SRDF must be loaded for KDL kinematics solver to work.");
    return false;
  }
  
  robot_model_.reset(new robot_model::RobotModel(urdf_model, srdf));
  joint_model_group_ = robot_model_->getJointModelGroup(group_name);
  if (!joint_model_group_) 
  {
    ROS_FATAL_STREAM_NAMED(NAME_, "Error in extracting the joint_model from the robot model (group name: "<< group_name <<")" );
    return false;
  }
  
  if (!kdl_parser::treeFromUrdfModel( *urdf_model, tree_)) 
  {
      ROS_FATAL_STREAM_NAMED(NAME_,"Failed to construct kdl tree");
      return false;
  }
  if (!tree_.getChain(IKFAST_BASE_FRAME_, IKFAST_EE_FRAME_, chain_))
  {
      ROS_FATAL_NAMED(NAME_,"Couldn't find chain from %s to %s", IKFAST_BASE_FRAME_.c_str(), IKFAST_EE_FRAME_.c_str());
      return false;
  }
  if (chain_.getNrOfJoints() != getNumJoints() )
  {
      ROS_FATAL_NAMED(NAME_,"weird .. mismatch between the number of joints in KDL  and the expected one (frame %s to %s)", IKFAST_BASE_FRAME_.c_str(), IKFAST_EE_FRAME_.c_str());
      return false;
  }

  ROS_DEBUG_STREAM_NAMED(NAME_, "Reading joints and links from URDF");

  urdf::LinkConstSharedPtr link = urdf_model->getLink(getTipFrame());
  while (link->name != base_frame_ && joint_names_.size() <= getNumJoints( ))
  {
    ROS_DEBUG_NAMED(NAME_, "Link %s", link->name.c_str());
    link_names_.push_back(link->name);
    urdf::JointSharedPtr joint = link->parent_joint;
    if (joint)
    {
      all_joint_names_.push_back(joint->name);
      all_link_names_.push_back(link->name);
      if (joint->type != urdf::Joint::UNKNOWN && joint->type != urdf::Joint::FIXED)
      {
        ROS_DEBUG_STREAM_NAMED(NAME_, "Adding joint " << joint->name);

        joint_names_.push_back(joint->name);
        float lower, upper;
        int hasLimits;
        if (joint->type != urdf::Joint::CONTINUOUS)
        {
          if (joint->safety)
          {
            lower = joint->safety->soft_lower_limit;
            upper = joint->safety->soft_upper_limit;
          }
          else
          {
            lower = joint->limits->lower;
            upper = joint->limits->upper;
          }
          hasLimits = 1;
        }
        else
        {
          lower = -M_PI;
          upper = M_PI;
          hasLimits = 0;
        }
        if (hasLimits)
        {
          joint_has_limits_vector_.push_back(true);
          joint_min_vector_.push_back(lower);
          joint_max_vector_.push_back(upper);
        }
        else
        {
          joint_has_limits_vector_.push_back(false);
          joint_min_vector_.push_back(-M_PI);
          joint_max_vector_.push_back(M_PI);
        }
      }
    }
    else
    {
      ROS_WARN_NAMED(NAME_, "no joint corresponding to %s", link->name.c_str());
    }
    link = link->getParent();
  }

  if (joint_names_.size() != getNumJoints( ))
  {
    ROS_FATAL_STREAM_NAMED(NAME_, "Joint numbers mismatch: URDF has " << joint_names_.size() << " and IKFast has "
                                                                      << getNumJoints( ));
    return false;
  }

  std::reverse(all_link_names_.begin(),all_link_names_.end());
  std::reverse(all_joint_names_.begin(),all_joint_names_.end());
  std::reverse(link_names_.begin(), link_names_.end());
  std::reverse(joint_names_.begin(), joint_names_.end());
  std::reverse(joint_min_vector_.begin(), joint_min_vector_.end());
  std::reverse(joint_max_vector_.begin(), joint_max_vector_.end());
  std::reverse(joint_has_limits_vector_.begin(), joint_has_limits_vector_.end());

  for (size_t i = 0; i < getNumJoints( ); ++i)
    ROS_DEBUG_STREAM_NAMED(NAME_, joint_names_[i] << " " << joint_min_vector_[i] << " " << joint_max_vector_[i] << " "
                                                  << joint_has_limits_vector_[i]);
  
  moveit::core::RobotState  rs( robot_model_ );
  // 0: world frame
  // gb: group base frame
  // gt: group tip  frame
  // ib: ikfast base frame (i.e., autogenerated file in such frame)
  // it: ikfast tip frame (i.e., autogenerated file in such frame)
  Eigen::Affine3d T_0_gb = rs.getGlobalLinkTransform(getBaseFrame());
  Eigen::Affine3d T_0_ib = rs.getGlobalLinkTransform(IKFAST_BASE_FRAME_);
  
  Eigen::Affine3d T_0_gt = rs.getGlobalLinkTransform(getTipFrame());
  Eigen::Affine3d T_0_it = rs.getGlobalLinkTransform(IKFAST_EE_FRAME_);
  
  T_gb_ib_ = T_0_gb.inverse() * T_0_ib;
  T_gt_it_ = T_0_gt.inverse() * T_0_it;
  /*
  
  ROS_INFO_STREAM("Reference Frames ====== \n group base (gb): " << getBaseFrame() << ", ikfast base (ib): " << IKFAST_BASE_FRAME_  << ",  group tip (gt): " << getTipFrame() << ", ikfast tip (it): " << IKFAST_EE_FRAME_ );
  std::cout << "T_gb_ib:\n";
  std::cout << "Translation: "  <<T_gb_ib_.translation().transpose() << std::endl;;
  std::cout << "Rotation: "     <<std::endl << T_gb_ib_.linear() << std::endl;
  
  
  std::cout << "\nT_gt_it ";
  std::cout << "Translation: " << T_gt_it_.translation().transpose() << std::endl;
  std::cout << "Rotation: " <<std::endl << T_gt_it_.linear() << std::endl;
  ROS_INFO("End ================================================================================= ");
  */

  active_ = true;
  return true;
}

void IKFastKinematicsPlugin::setSearchDiscretization(const std::map<int, double>& discretization)
{
  if (discretization.empty())
  {
    ROS_ERROR("The 'discretization' map is empty");
    return;
  }

  if (redundant_joint_indices_.empty())
  {
    ROS_ERROR_STREAM("This group's solver doesn't support redundant joints");
    return;
  }
  
}

bool IKFastKinematicsPlugin::setRedundantJoints(const std::vector<unsigned int>& redundant_joint_indices)
{
  ROS_ERROR_STREAM("Changing the redundant joints isn't permitted by this group's solver ");
  return false;
}

int IKFastKinematicsPlugin::solve ( KDL::Frame&                     pose_frame
                                  , const std::vector<double>&      vfree
                                  , ikfast::IkSolutionList<double>& solutions) const
{
  // IKFast56/61
  solutions.Clear();
  
  double roll, pitch, yaw;
  pose_frame.M.GetRPY(roll, pitch, yaw);
  ComputeIk( pose_frame.p[0], pose_frame.p[1], pose_frame.p[2], -pitch, solutions);
  return solutions.GetNumSolutions();
  
}

void IKFastKinematicsPlugin::getSolution(const ikfast::IkSolutionList<double>& solutions, int i,
                                         std::vector<double>& solution) const
{
  solution.clear();
  solution.resize(getNumJoints( ));

  // IKFast56/61
  const ikfast::IkSolutionBase<double>& sol = solutions.GetSolution(i);
  std::vector<double> vsolfree(sol.GetFree().size());
  sol.GetSolution(&solution[0], vsolfree.size() > 0 ? &vsolfree[0] : NULL);

  // std::cout << "solution " << i << ":" ;
  // for(int j=0;j<getNumJoints( ); ++j)
  //   std::cout << " " << solution[j];
  // std::cout << std::endl;

  // ROS_ERROR("%f %d",solution[2],vsolfree.size());
}

void IKFastKinematicsPlugin::getSolution(const ikfast::IkSolutionList<double>& solutions,
                                         const std::vector<double>& ik_seed_state, int i,
                                         std::vector<double>& solution) const
{
  solution.clear();
  solution.resize(getNumJoints( ));

  // IKFast56/61
  const ikfast::IkSolutionBase<double>& sol = solutions.GetSolution(i);
  std::vector<double> vsolfree(sol.GetFree().size());
  sol.GetSolution(&solution[0], vsolfree.size() > 0 ? &vsolfree[0] : NULL);

  // rotate joints by +/-360° where it is possible and useful
  for (std::size_t i = 0; i < getNumJoints( ); ++i)
  {
    if (joint_has_limits_vector_[i])
    {
      double signed_distance = solution[i] - ik_seed_state[i];
      while (signed_distance > M_PI && solution[i] - 2 * M_PI > (joint_min_vector_[i] - LIMIT_TOLERANCE))
      {
        signed_distance -= 2 * M_PI;
        solution[i] -= 2 * M_PI;
      }
      while (signed_distance < -M_PI && solution[i] + 2 * M_PI < (joint_max_vector_[i] + LIMIT_TOLERANCE))
      {
        signed_distance += 2 * M_PI;
        solution[i] += 2 * M_PI;
      }
    }
  }
}

double IKFastKinematicsPlugin::harmonize(const std::vector<double>& ik_seed_state, std::vector<double>& solution) const
{
  double dist_sqr = 0;
  std::vector<double> ss = ik_seed_state;
  for (size_t i = 0; i < ik_seed_state.size(); ++i)
  {
    while (ss[i] > 2 * M_PI)
    {
      ss[i] -= 2 * M_PI;
    }
    while (ss[i] < 2 * M_PI)
    {
      ss[i] += 2 * M_PI;
    }
    while (solution[i] > 2 * M_PI)
    {
      solution[i] -= 2 * M_PI;
    }
    while (solution[i] < 2 * M_PI)
    {
      solution[i] += 2 * M_PI;
    }
    dist_sqr += fabs(ik_seed_state[i] - solution[i]);
  }
  return dist_sqr;
}

void IKFastKinematicsPlugin::getClosestSolution(const ikfast::IkSolutionList<double>& solutions,
                                                const std::vector<double>& ik_seed_state,
                                                std::vector<double>& solution) const
{
  double mindist = DBL_MAX;
  int minindex = -1;
  std::vector<double> sol;

  // IKFast56/61
  for (size_t i = 0; i < solutions.GetNumSolutions(); ++i)
  {
    getSolution(solutions, i, sol);
    double dist = harmonize(ik_seed_state, sol);
    ROS_INFO_STREAM_NAMED(NAME_, "Dist " << i << " dist " << dist);
    // std::cout << "dist[" << i << "]= " << dist << std::endl;
    if (minindex == -1 || dist < mindist)
    {
      minindex = i;
      mindist = dist;
    }
  }
  if (minindex >= 0)
  {
    getSolution(solutions, minindex, solution);
    harmonize(ik_seed_state, solution);
  }
}


bool IKFastKinematicsPlugin::getCount(int& count, const int& max_count, const int& min_count) const
{
  if (count > 0)
  {
    if (-count >= min_count)
    {
      count = -count;
      return true;
    }
    else if (count + 1 <= max_count)
    {
      count = count + 1;
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    if (1 - count <= max_count)
    {
      count = 1 - count;
      return true;
    }
    else if (count - 1 >= min_count)
    {
      count = count - 1;
      return true;
    }
    else
      return false;
  }
}

bool IKFastKinematicsPlugin::getPositionFK(const std::vector<std::string>& link_names,
                                           const std::vector<double>& joint_angles,
                                           std::vector<geometry_msgs::Pose>& poses) const
{
  KDL::Frame p_out;
  if (link_names.size() == 0)
  {
    ROS_WARN_STREAM_NAMED(NAME_, "Link names with nothing");
    return false;
  }

  if (link_names.size() != 1 || link_names[0] != getTipFrame())
  {
    ROS_ERROR_NAMED(NAME_, "Can compute FK for %s only, while the request is for the link %s", getTipFrame().c_str(), link_names[0].c_str() );
    return false;
  }

  if (joint_angles.size() != getNumJoints( ))
  {
    ROS_ERROR_NAMED(NAME_, "Unexpected number of joint angles");
    return false;
  }

  Eigen::Affine3d T_ib_it;
  ComputeFk( VectorXd::Map(joint_angles.data(), joint_angles.size() ), T_ib_it);

  Eigen::Affine3d T_gb_gt = T_gb_ib_ * T_ib_it * T_gt_it_.inverse();
  
  poses.resize(1);
  tf::poseEigenToMsg(T_gb_gt, poses[0]);
  
  return true;
}

bool IKFastKinematicsPlugin::searchPositionIK(const geometry_msgs::Pose& ik_pose,
                                              const std::vector<double>& ik_seed_state, double timeout,
                                              std::vector<double>& solution, moveit_msgs::MoveItErrorCodes& error_code,
                                              const kinematics::KinematicsQueryOptions& options) const
{
  const IKCallbackFn solution_callback = 0;
  std::vector<double> consistency_limits;

  return searchPositionIK(ik_pose, ik_seed_state, timeout, consistency_limits, solution, solution_callback, error_code,
                          options);
}

bool IKFastKinematicsPlugin::searchPositionIK(const geometry_msgs::Pose& ik_pose,
                                              const std::vector<double>& ik_seed_state, double timeout,
                                              const std::vector<double>& consistency_limits,
                                              std::vector<double>& solution, moveit_msgs::MoveItErrorCodes& error_code,
                                              const kinematics::KinematicsQueryOptions& options) const
{
  const IKCallbackFn solution_callback = 0;
  return searchPositionIK(ik_pose, ik_seed_state, timeout, consistency_limits, solution, solution_callback, error_code,
                          options);
}

bool IKFastKinematicsPlugin::searchPositionIK(const geometry_msgs::Pose& ik_pose,
                                              const std::vector<double>& ik_seed_state, double timeout,
                                              std::vector<double>& solution, const IKCallbackFn& solution_callback,
                                              moveit_msgs::MoveItErrorCodes& error_code,
                                              const kinematics::KinematicsQueryOptions& options) const
{
  std::vector<double> consistency_limits;
  return searchPositionIK(ik_pose, ik_seed_state, timeout, consistency_limits, solution, solution_callback, error_code,
                          options);
}

bool IKFastKinematicsPlugin::searchPositionIK(const geometry_msgs::Pose& ik_pose,
                                              const std::vector<double>& ik_seed_state, double timeout,
                                              const std::vector<double>& consistency_limits,
                                              std::vector<double>& solution, const IKCallbackFn& solution_callback,
                                              moveit_msgs::MoveItErrorCodes& error_code,
                                              const kinematics::KinematicsQueryOptions& options) const
{
  ROS_DEBUG_STREAM_NAMED(NAME_, "searchPositionIK");

  /// search_mode is currently fixed during code generation
  SEARCH_MODE search_mode = OPTIMIZE_MAX_JOINT;

  // Check if there are no redundant joints
  if (free_params_.size() == 0)
  {
    ROS_DEBUG_STREAM_NAMED(NAME_, "No need to search since no free params/redundant joints");

    std::vector<geometry_msgs::Pose> ik_poses(1, ik_pose);
    std::vector<std::vector<double>> solutions;
    kinematics::KinematicsResult kinematic_result;
    // Find all IK solution within joint limits
    if (!getPositionIK(ik_poses, ik_seed_state, solutions, kinematic_result, options))
    {
      ROS_DEBUG_STREAM_NAMED(NAME_, "No solution whatsoever");
      error_code.val = moveit_msgs::MoveItErrorCodes::NO_IK_SOLUTION;
      return false;
    }

    // sort solutions by their distance to the seed
    std::vector<LimitObeyingSol> solutions_obey_limits;
    for (std::size_t i = 0; i < solutions.size(); ++i)
    {
      double dist_from_seed = 0.0;
      for (std::size_t j = 0; j < ik_seed_state.size(); ++j)
      {
        dist_from_seed += fabs(ik_seed_state[j] - solutions[i][j]);
      }

      solutions_obey_limits.push_back({ solutions[i], dist_from_seed });
    }
    std::sort(solutions_obey_limits.begin(), solutions_obey_limits.end());

    // check for collisions if a callback is provided
    if (!solution_callback.empty())
    {
      for (std::size_t i = 0; i < solutions_obey_limits.size(); ++i)
      {
        solution_callback(ik_pose, solutions_obey_limits[i].value, error_code);
        if (error_code.val == moveit_msgs::MoveItErrorCodes::SUCCESS)
        {
          solution = solutions_obey_limits[i].value;
          ROS_DEBUG_STREAM_NAMED(NAME_, "Solution passes callback");
          return true;
        }
      }

      ROS_DEBUG_STREAM_NAMED(NAME_, "Solution has error code " << error_code);
      return false;
    }
    else
    {
      solution = solutions_obey_limits[0].value;
      error_code.val = moveit_msgs::MoveItErrorCodes::SUCCESS;
      return true;  // no collision check callback provided
    }
  }

  // -------------------------------------------------------------------------------------------------
  // Error Checking
  if (!active_)
  {
    ROS_ERROR_STREAM_NAMED(NAME_, "Kinematics not active");
    error_code.val = error_code.NO_IK_SOLUTION;
    return false;
  }

  if (ik_seed_state.size() != getNumJoints( ))
  {
    ROS_ERROR_STREAM_NAMED(NAME_, "Seed state must have size " << getNumJoints( ) << " instead of size "
                                                               << ik_seed_state.size());
    error_code.val = error_code.NO_IK_SOLUTION;
    return false;
  }

  if (!consistency_limits.empty() && consistency_limits.size() != getNumJoints( ))
  {
    ROS_ERROR_STREAM_NAMED(NAME_, "Consistency limits be empty or must have size " << getNumJoints( ) << " instead of size "
                                                                                   << consistency_limits.size());
    error_code.val = error_code.NO_IK_SOLUTION;
    return false;
  }

  // -------------------------------------------------------------------------------------------------
  // Initialize

  KDL::Frame frame;
  tf::poseMsgToKDL(ik_pose, frame);

  std::vector<double> vfree(free_params_.size());

  ros::Time maxTime = ros::Time::now() + ros::Duration(timeout);
  int counter = 0;

  double initial_guess = ik_seed_state[free_params_[0]];
  vfree[0] = initial_guess;

  // -------------------------------------------------------------------------------------------------
  // Handle consitency limits if needed
  int num_positive_increments;
  int num_negative_increments;

  if (!consistency_limits.empty())
  {
    // moveit replaced consistency_limit (scalar) w/ consistency_limits (vector)
    // Assume [0]th free_params element for now.  Probably wrong.
    double max_limit = fmin(joint_max_vector_[free_params_[0]], initial_guess + consistency_limits[free_params_[0]]);
    double min_limit = fmax(joint_min_vector_[free_params_[0]], initial_guess - consistency_limits[free_params_[0]]);

    num_positive_increments = (int)((max_limit - initial_guess) / search_discretization_);
    num_negative_increments = (int)((initial_guess - min_limit) / search_discretization_);
  }
  else  // no consitency limits provided
  {
    num_positive_increments = (joint_max_vector_[free_params_[0]] - initial_guess) / search_discretization_;
    num_negative_increments = (initial_guess - joint_min_vector_[free_params_[0]]) / search_discretization_;
  }

  // -------------------------------------------------------------------------------------------------
  // Begin searching

  ROS_DEBUG_STREAM_NAMED(NAME_, "Free param is " << free_params_[0] << " initial guess is " << initial_guess
                                                 << ", # positive increments: " << num_positive_increments
                                                 << ", # negative increments: " << num_negative_increments);
  if ((search_mode & OPTIMIZE_MAX_JOINT) && (num_positive_increments + num_negative_increments) > 1000)
    ROS_WARN_STREAM_ONCE_NAMED(NAME_, "Large search space, consider increasing the search discretization");

  double best_costs = -1.0;
  std::vector<double> best_solution;
  int nattempts = 0, nvalid = 0;

  while (true)
  {
    ikfast::IkSolutionList<double> solutions;
    int numsol = solve(frame, vfree, solutions);

    ROS_DEBUG_STREAM_NAMED(NAME_, "Found " << numsol << " solutions from IKFast");

    // ROS_INFO("%f",vfree[0]);

    if (numsol > 0)
    {
      for (int s = 0; s < numsol; ++s)
      {
        nattempts++;
        std::vector<double> sol;
        getSolution(solutions, ik_seed_state, s, sol);

        bool obeys_limits = true;
        for (unsigned int i = 0; i < sol.size(); i++)
        {
          if (joint_has_limits_vector_[i] && (sol[i] < joint_min_vector_[i] || sol[i] > joint_max_vector_[i]))
          {
            obeys_limits = false;
            break;
          }
        }
        if (obeys_limits)
        {
          getSolution(solutions, ik_seed_state, s, solution);

          // This solution is within joint limits, now check if in collision (if callback provided)
          if (!solution_callback.empty())
          {
            solution_callback(ik_pose, solution, error_code);
          }
          else
          {
            error_code.val = error_code.SUCCESS;
          }

          if (error_code.val == error_code.SUCCESS)
          {
            nvalid++;
            if (search_mode & OPTIMIZE_MAX_JOINT)
            {
              // Costs for solution: Largest joint motion
              double costs = 0.0;
              for (unsigned int i = 0; i < solution.size(); i++)
              {
                double d = fabs(ik_seed_state[i] - solution[i]);
                if (d > costs)
                  costs = d;
              }
              if (costs < best_costs || best_costs == -1.0)
              {
                best_costs = costs;
                best_solution = solution;
              }
            }
            else
              // Return first feasible solution
              return true;
          }
        }
      }
    }

    if (!getCount(counter, num_positive_increments, -num_negative_increments))
    {
      // Everything searched
      error_code.val = moveit_msgs::MoveItErrorCodes::NO_IK_SOLUTION;
      break;
    }

    vfree[0] = initial_guess + search_discretization_ * counter;
    // ROS_DEBUG_STREAM_NAMED(NAME_,"Attempt " << counter << " with 0th free joint having value " << vfree[0]);
  }

  ROS_DEBUG_STREAM_NAMED(NAME_, "Valid solutions: " << nvalid << "/" << nattempts);

  if ((search_mode & OPTIMIZE_MAX_JOINT) && best_costs != -1.0)
  {
    solution = best_solution;
    error_code.val = error_code.SUCCESS;
    return true;
  }

  // No solution found
  error_code.val = moveit_msgs::MoveItErrorCodes::NO_IK_SOLUTION;
  return false;
}

// Used when there are no redundant joints - aka no free params
bool IKFastKinematicsPlugin::getPositionIK( const geometry_msgs::Pose& ik_pose
                                          , const std::vector<double>& ik_seed_state
                                          , std::vector<double>& solution
                                          , moveit_msgs::MoveItErrorCodes& error_code
                                          , const kinematics::KinematicsQueryOptions& options) const
{
  ROS_DEBUG_STREAM_NAMED(NAME_, "getPositionIK");

  if (!active_)
  {
    ROS_ERROR("kinematics not active");
    return false;
  }

  if (ik_seed_state.size() < getNumJoints( ))
  {
    ROS_ERROR_STREAM("ik_seed_state only has " << ik_seed_state.size() << " entries, this ikfast solver requires "
                                               << getNumJoints( ));
    return false;
  }

  // Check if seed is in bound
  for (std::size_t i = 0; i < ik_seed_state.size(); i++)
  {
    // Add tolerance to limit check
    if (joint_has_limits_vector_[i] && ((ik_seed_state[i] < (joint_min_vector_[i] - LIMIT_TOLERANCE)) ||
                                        (ik_seed_state[i] > (joint_max_vector_[i] + LIMIT_TOLERANCE))))
    {
      ROS_ERROR_STREAM_NAMED("ikseed", "Not in limits! Seed of Ax. " << (int)i << " is " << ik_seed_state[i]
                                                         << ", while the axis has limit: " << joint_has_limits_vector_[i] << " ["
                                                         << joint_min_vector_[i] << " to " << joint_max_vector_[i] <<"]");
      return false;
    }
  }

  std::vector<double> vfree(free_params_.size());
  for (std::size_t i = 0; i < free_params_.size(); ++i)
  {
    int p = free_params_[i];
    ROS_ERROR("%u is %f", p, ik_seed_state[p]);  // DTC
    vfree[i] = ik_seed_state[p];
  }
  
  moveit::core::RobotState  rs( robot_model_ );
  Eigen::Affine3d           T_gb_gt; 
  tf::poseMsgToEigen( ik_pose, T_gb_gt );
  
  Eigen::Affine3d T_ib_it = T_gb_ib_.inverse() * T_gb_gt * T_gt_it_;
  
  KDL::Frame frame; 
  tf::transformEigenToKDL(T_ib_it,frame);

  ikfast::IkSolutionList<double> solutions;
  
  int numsol = solve(frame, vfree, solutions);
  ROS_DEBUG_STREAM_NAMED(NAME_, "Found " << numsol << " solutions from IKFast");

  std::vector<LimitObeyingSol> solutions_obey_limits;

  if (numsol)
  {
    std::vector<double> solution_obey_limits;
    for (std::size_t s = 0; s < numsol; ++s)
    {
      std::vector<double> sol;
      getSolution(solutions, ik_seed_state, s, sol);
      ROS_DEBUG_NAMED(NAME_, "Sol %d: %e   %e   %e   %e   %e   %e", (int)s, sol[0], sol[1], sol[2], sol[3], sol[4],
                      sol[5]);

      bool obeys_limits = true;
      for (std::size_t i = 0; i < sol.size(); i++)
      {
        // Add tolerance to limit check
        if (joint_has_limits_vector_[i] && ((sol[i] < (joint_min_vector_[i] - LIMIT_TOLERANCE)) ||
                                            (sol[i] > (joint_max_vector_[i] + LIMIT_TOLERANCE))))
        {
          // One element of solution is not within limits
          obeys_limits = false;
          ROS_DEBUG_STREAM_NAMED(NAME_, "Not in limits! The axis" << (int)i+1 << " out of " << sol.size() 
                                                          << " should be at" << sol[i] << ", while it has limit: "
                                                          << joint_has_limits_vector_[i] << "  being  "
                                                          << joint_min_vector_[i] << " to " << joint_max_vector_[i]);
          break;
        }
      }
      if (obeys_limits)
      {
        // All elements of this solution obey limits
        getSolution(solutions, ik_seed_state, s, solution_obey_limits);
        double dist_from_seed = 0.0;
        for (std::size_t i = 0; i < ik_seed_state.size(); ++i)
        {
          dist_from_seed += fabs(ik_seed_state[i] - solution_obey_limits[i]);
        }

        solutions_obey_limits.push_back({ solution_obey_limits, dist_from_seed });
      }
    }
  }
  else
  {
    ROS_ERROR_STREAM_NAMED(NAME_, "No IK solution");
  }

  // Sort the solutions under limits and find the one that is closest to ik_seed_state
  if (!solutions_obey_limits.empty())
  {
    std::sort(solutions_obey_limits.begin(), solutions_obey_limits.end());
    solution = solutions_obey_limits[0].value;
    error_code.val = moveit_msgs::MoveItErrorCodes::SUCCESS;
    return true;
  }

  error_code.val = moveit_msgs::MoveItErrorCodes::NO_IK_SOLUTION;
  return false;
}

bool IKFastKinematicsPlugin::getPositionIK(const std::vector<geometry_msgs::Pose>& ik_poses,
                                           const std::vector<double>& ik_seed_state,
                                           std::vector<std::vector<double>>& solutions,
                                           kinematics::KinematicsResult& result,
                                           const kinematics::KinematicsQueryOptions& options) const
{
  ROS_DEBUG_STREAM_NAMED(NAME_, "getPositionIK with multiple solutions");

  if (!active_)
  {
    ROS_ERROR("kinematics not active");
    result.kinematic_error = kinematics::KinematicErrors::SOLVER_NOT_ACTIVE;
    return false;
  }

  if (ik_poses.empty())
  {
    ROS_ERROR("ik_poses is empty");
    result.kinematic_error = kinematics::KinematicErrors::EMPTY_TIP_POSES;
    return false;
  }

  if (ik_poses.size() > 1)
  {
    ROS_ERROR("ik_poses contains multiple entries, only one is allowed");
    result.kinematic_error = kinematics::KinematicErrors::MULTIPLE_TIPS_NOT_SUPPORTED;
    return false;
  }

  if (ik_seed_state.size() < getNumJoints( ))
  {
    ROS_ERROR_STREAM("ik_seed_state only has " << ik_seed_state.size() << " entries, this ikfast solver requires "
                                               << getNumJoints( ));
    return false;
  }
  
  
  moveit::core::RobotState  rs( robot_model_ );
  Eigen::Affine3d           T_gb_gt; 
  tf::poseMsgToEigen( ik_poses[0], T_gb_gt );
  
  Eigen::Affine3d T_ib_it = T_gb_ib_.inverse() * T_gb_gt * T_gt_it_;
  
  KDL::Frame frame; 
  tf::transformEigenToKDL(T_ib_it,frame);


  // solving ik
  std::vector<ikfast::IkSolutionList<double>> solution_set;
  ikfast::IkSolutionList<double> ik_solutions;
  std::vector<double> vfree;
  int numsol = 0;
  std::vector<double> sampled_joint_vals;

  // computing for single solution set
  numsol = solve(frame, vfree, ik_solutions);
  solution_set.push_back(ik_solutions);

  ROS_DEBUG_STREAM_NAMED(NAME_, "Found " << numsol << " solutions from IKFast");
  bool solutions_found = false;
  if (numsol > 0)
  {
    /*
      Iterating through all solution sets and storing those that do not exceed joint limits.
    */
    for (unsigned int r = 0; r < solution_set.size(); r++)
    {
      ik_solutions = solution_set[r];
      numsol = ik_solutions.GetNumSolutions();
      for (int s = 0; s < numsol; ++s)
      {
        std::vector<double> sol;
        getSolution(ik_solutions, ik_seed_state, s, sol);

        bool obeys_limits = true;
        for (unsigned int i = 0; i < sol.size(); i++)
        {
          // Add tolerance to limit check
          if (joint_has_limits_vector_[i] && ((sol[i] < (joint_min_vector_[i] - LIMIT_TOLERANCE)) ||
                                              (sol[i] > (joint_max_vector_[i] + LIMIT_TOLERANCE))))
          {
            // One element of solution is not within limits
            obeys_limits = false;
            ROS_DEBUG_STREAM_NAMED(NAME_, "Not in limits! " << i << " value " << sol[i] << " has limit: "
                                                            << joint_has_limits_vector_[i] << "  being  "
                                                            << joint_min_vector_[i] << " to " << joint_max_vector_[i]);
            break;
          }
        }
        if (obeys_limits)
        {
          // All elements of solution obey limits
          solutions_found = true;
          solutions.push_back(sol);
        }
      }
    }

    if (solutions_found)
    {
      result.kinematic_error = kinematics::KinematicErrors::OK;
      return true;
    }
  }
  else
  {
    ROS_DEBUG_STREAM_NAMED(NAME_, "No IK solution");
  }

  result.kinematic_error = kinematics::KinematicErrors::NO_SOLUTION;
  return false;
}

bool IKFastKinematicsPlugin::sampleRedundantJoint(kinematics::DiscretizationMethod method,
                                                  std::vector<double>& sampled_joint_vals) const
{
  
  if (redundant_joint_indices_.empty())
  {
    ROS_ERROR_STREAM("This group's solver doesn't support redundant joints");
  }

  return false;
}


}  // end namespace

// register IKFastKinematicsPlugin as a KinematicsBase implementation
#include <pluginlib/class_list_macros.hpp>
PLUGINLIB_EXPORT_CLASS(ikfast_kinematics_plugin::IKFastKinematicsPlugin, kinematics::KinematicsBase);



int main( int argc, char* argv[] ) 
{
  ros::init( argc , argv, "test_kin_popeye" );
  ros::NodeHandle nh("~");
  ros::AsyncSpinner spinner(4);
  spinner.start();
  
  ROS_INFO("Check verbosity level (%s)....... ", (ros::NodeHandle("~").getNamespace() + "/verbose").c_str()  );
  if( ros::NodeHandle("~").hasParam("verbose") )
  {
    ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug);
  }
  
  ikfast_kinematics_plugin::IKFastKinematicsPlugin kin;
  kin.initialize("robot_description", "popeye_arm", "popeye_link_0","popeye_flange", 0.01);
  
  std::vector<std::string> link_names{"popeye_flange"};
  std::srand (time(NULL));

  std::vector< std::vector<double> > set_of_joint_angles =  { {0.0,0.0,0.0,0.0}
                                                            , {0.0,0.1,0.1,0.0}
                                                            , {0.0,double(std::rand())/RAND_MAX,double(std::rand())/RAND_MAX,0.0}
    
  };
  for( const auto & joint_angles: set_of_joint_angles )
  {
    std::cout << "====================================" << std::endl;
    std::cout << "IN q: ";  for( const auto & j : joint_angles ) std::cout << j <<",\t"; std::cout << std::endl;
    std::vector<geometry_msgs::Pose> poses;
    if(! kin.getPositionFK(link_names, joint_angles, poses) )
    {
      ROS_ERROR("UFFA");
      continue;
    }
    std::cout << "OUT fk: "; for( const auto & pose : poses ) std::cout << pose << std::endl;

    std::vector<double> solution(4,0); 
    moveit_msgs::MoveItErrorCodes error_code;
    if(!kin.getPositionIK(poses[0], joint_angles, solution, error_code) )
    {
      ROS_ERROR("UFFA");
      continue;
    }
    
    std::cout << "OUT q: ";  for( const auto & j : solution ) std::cout << j <<",\t" ; std::cout << std::endl;
  }
  
  return 1;
}
